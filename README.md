# Daily Bing Wallpaper for Linux Mint
This script will download the Bing Wallpaper of the day from random region.
Multi user support to avoid users of the same computer having the same wallpaper.
It should work on others distros, but I tested only on Mint 18.3 and 19.3

IMPORTANT READ THOSE "IMPORTANT" LINES IN THE getbing script
1) Change the path in the script 
2) Change the usernames in the script

HOW TO USE: 
Create a /path/to/getbing.sh entry in your "apps launched at startup" list.

Set the yourusername.jpg image as your wallpaper. 

Everyday it will change when you login.



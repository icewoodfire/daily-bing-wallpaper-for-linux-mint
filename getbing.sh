#!/bin/bash

# ****** IMPORTANT READ THOSE "IMPORTANT" LINES BELOW! ********************************
# ****** IMPORTANT: CHANGE THE PATH ON NEXT LINE **************************************

cd ~/Pictures/bing
if [ "$1" = "force" ] 
then
  echo ok1
elif test `find "$USER.jpg" -mmin -720` 
then
  echo enough recent found
  exit 1
fi
BING_MARKETS=("ar-XA" "bg-BG" "cs-CZ" "da-DK" "de-AT" "de-CH" "de-DE" "el-GR" "en-A" "en-CA" "en-GB" "en-ID" "en-IE" "en-IN" "en-MY" "en-NZ" "en-PH" "en-SG" "en-US" "en-XA" "en-ZA" "es-AR" "es-CL" "es-ES" "es-MX" "es-US" "es-XL" "et-EE" "fi-FI" "fr-BE" "fr-CA" "fr-CH" "fr-FR" "he-IL" "hr-HR" "hu-H" "it-IT" "ja-JP" "ko-KR" "lt-LT" "lv-LV" "nb-NO" "nl-BE" "nl-NL" "pl-PL" "pt-BR" "pt-PT" "ro-RO" "ru-R" "sk-SK" "sl-SL" "sv-SE" "th-TH" "tr-TR" "uk-UA" "zh-CN" "zh-HK" "zh-TW")
# seed random generator
RANDOM=$$$(date +%s)
# pick a random entry from the BING MARKET array
market=${BING_MARKETS[$RANDOM % ${#BING_MARKETS[@]} ]}
echo market=$market
days=0 #DEFAULT for any other users

# ****** IMPORTANT: CHANGE username1, username2, username3  BELOW ****************************

if [ $USER = "username1" ] ; then
    days=2
elif [ $USER = "username2" ]; then
    days=4
elif [ $USER = "username3" ]; then
    days=6
fi
url='http://www.bing.com/HPImageArchive.aspx?format=xml&idx='$days'&n=1&mkt='$market
echo url=$url
img=$(wget -qO - $url | sed 's:<url>:\n:g' |   sed 's:</url>:\n:g' |   sed 's:&amp;:\n:g' | grep 1920x1080 | head -n 1)
DATE=`date +%Y-%m-%d`
echo DATE=$DATE
[ -z "$img" ] && exit 1
wget -qN bing.com$img
img=$(echo $img | sed 's:.*/::')
cp $img $USER.jpg
rm $img


